# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------
# If you submit this package back to Spack as a pull request,
# please first remove this boilerplate and all FIXME comments.
#
# This is a template package file for Spack.  We've put "FIXME"
# next to all the things you'll want to change. Once you've handled
# them, you can save this file and test your package like this:
#
#     spack install py-carputils
#
# You can edit this file again by typing:
#
#     spack edit py-carputils
#
# See the Spack documentation for more information on packaging.
# ----------------------------------------------------------------------------

from spack import *


class PyCarputils(PythonPackage):
    """The carputils framework for running simulations with the openCARP software."""

    homepage = "https://www.opencarp.org"
    git      = "https://git.opencarp.org/openCARP/carputils.git"

    # Add a list of GitHub accounts to
    # notify when the package is updated.
    maintainers = ['MarieHouillon']

    version('master', branch='master')
    # Version to use with openCARP 7.0
    version('oc7.0', commit='4c04db61744f2fb7665594d7c810699c5c55c77c')

    depends_on('git')
    
    depends_on('python@2.7:2.8,3.6:3.8', type=('build', 'run'))
    depends_on('py-pip', type='build')
    depends_on('py-numpy@1.14.5:', type=('build', 'run'))
    depends_on('py-setuptools', type='build')
    depends_on('py-python-dateutil', type='run')
    depends_on('py-scipy@:1.5.4', type='run')
    depends_on('py-matplotlib@:3.3.3', type='run')
    depends_on('py-pandas@:1.1.4', type='run')
    depends_on('py-tables@3.6.1', type='run')
    depends_on('py-six@:1.14.0', type='run')
    depends_on('py-ruamel-yaml@0.17.4:', type='run')
