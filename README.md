# Create openCARP Spack package

Every contribution to improve these Spack packages is welcome.

## Organization of packages
I've made a package for meshtool, carputils (called py-carputils) and opencarp.
All of them can be installed as standalones, but I added variants in the openCARP package which allow to install openCARP, carputils and meshtool as a whole (as it is in openCARP releases).

This allows to install the complete package using a single command, and it also ensures that the settings file created for carputils contains the right executable paths for openCARP and meshtool.

## Getting started with Spack
```
git clone https://github.com/spack/spack.git
cd spack
git checkout releases/latest
```

Note: You can alternatively clone a fork of the Spack repository which already contains the openCARP packages:
```
git clone https://github.com/MarieHouillon/spack.git
cd spack
```

Then source Spack script:
```
. share/spack/setup-env.sh
```

Let Spack find the compilers available by using:
```
spack compilers
```

If the compiler you wish to use is loaded from a module (typical on a cluster), you should have to set this module in the `compilers.yaml` file. In my case, it is located at:
```
$HOME/.spack/linux/compilers.yaml
```

In there, the module-loaded compiler was found by Spack, but the `modules` field was empty, and I had to fill it manually: 
```
compilers:
- compiler:
    spec: gcc@10.2.0
    paths:
        cc: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/gcc
        cxx: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/g++
        f77: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/gfortran
        fc: /opt/bwhpc/common/compiler/gnu/10.2.0/bin/gfortran
    flags:  {}
    operating_system: rhel8
    target: x86_64
    modules: [compiler/gnu/10.2]
```

## Add openCARP repository to Spack

Note: if you cloned the Spack repository containing the openCARP packages, you should not follow the instructions in this section.

Then you have to get openCARP spack repository and add it to the list of Spack repos.

From the Spack directory, use for example:
```
cd ./var/spack/repos/
git clone https://git.opencarp.org/openCARP/opencarp-spack-packaging.git opencarp
```
Then add this repo to the Spack repos list: modify (or create) for example the file located at `$HOME/.spack/repos.yaml` to add the `opencarp` repo:
```
repos:
- $HOME/spack/var/spack/repos/opencarp
```

## Install openCARP
The simplest way to do it is to install the `opencarp` package with the `carputils` and `meshtool` variants:
```
spack install opencarp +carputils +meshtool
```

In order to get the `7.0` release:
```
spack install opencarp@7.0 +carputils +meshtool
```

Using this command line, opencarp, carputils and meshtool will be installed.

In addition, when the carputils settings file is created, the paths to openCARP and meshtool executables are found and automatically set.

Remark: the carputils settings file can be found at `$HOME/.config/carputils/settings.yaml`.

In order to install only `py-carputils`, you can execute:
```
spack install py-carputils
```

If you only want to install openCARP, use
```
spack install opencarp
```

If you only want to install meshtool:
```
spack install meshtool
```

## Using openCARP

In order to use openCARP, you have then to load it like it would be done for a module:
```
spack load opencarp
```

An if everything went well until now, you should be able to run simulations :)

## Some random points

- When building Spack packages, you don't have access to LD_LIBRARY_PATH. You can force the access to this variable by using the `--dirty` option when using `spack install`. Warning: this is not considered as a good practice.
 
### Problems encountered with the openCARP build process

There are problems with the build process of openCARP when trying to create a Spack package: some temporary modifications had to be made in order to create the Spack package.

- In the `postinst` phase: https://git.opencarp.org/openCARP/openCARP/-/blob/spack_tests/cmake/postinst

	- Spack sets its own `CMAKE_INSTALL_PREFIX` while the installation directory of openCARP is hardcoded in the `postinst` phase.
	- In addition, some symbolic links which are created require `root` privileges

- In https://git.opencarp.org/openCARP/openCARP/-/blob/spack_tests/simulator/build_info.py#L171, there is a need to give an absolute path to the `git()` function because Spack does not build from within the git repository.

- The `CPackConfig` file (https://git.opencarp.org/openCARP/openCARP/-/blob/spack_tests/cmake/CPackConfig.cmake) causes issues, I just removed its inclusion for now

In order to overcome these issues, I used a feature provided by Spack which allows to patch the source code before the installation. I created a patch file thanks to the command
```
git diff master..spack_tests > patch_file.patch
``` 
where `spack_tests` is the branch containing the necessary modifications. 

## Useful tips for spack

- To use some external packages instead of Spack packages, you can use one of two ways:
  1. Rely on the `spack external` command, e.g., `spack external find cmake`
  2. Modify `~/.spack/packages.yaml` or `$SPACK/etc/spack/packages.yaml` like in the following:
  ```
  packages:
    openmpi:
      externals:
      - spec: "openmpi@1.4.3%gcc@4.4.7 arch=linux-debian7-x86_64"
        prefix: /opt/openmpi-1.4.3
      - spec: "openmpi@1.4.3%gcc@4.4.7 arch=linux-debian7-x86_64+debug"
        prefix: /opt/openmpi-1.4.3-debug
    cmake:
      externals:
      - spec: cmake@3.7.2
        modules:
        - CMake/3.7.2
      buildable: False
  ```

